CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Installation


INTRODUCTION
------------

Current Maintainers:

* [yannickoo](http://drupal.org/user/531118)

Allows you to wrap Webform components with a HTML element.


INSTALLATION
------------

1. Install the module the drupal way [1]

2. Go to a webform and add a "Wrapper" component.

[1] http://drupal.org/documentation/install/modules-themes/modules-7
